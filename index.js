// import { AppRegistry } from 'react-native';


// AppRegistry.registerComponent('ProjectB', () => App);

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

// import bookReducer from './src/reducers/books';
// import authReducer from './src/reducers/auth';
// import AppWithNavigation from './src/service/app';
import AppWithNavigation from './src/services/app';


import AppReducer from './src/reducers';

// const rootReducer = combineReducers({ books: bookReducer, auth: authReducer });
// const rootReducer = combineReducers({ AppReducer });
const store = createStore(AppReducer, applyMiddleware(thunk, createLogger()));

class Biblioteca extends Component {
  a = () => {}
  render() {
    console.log('Biblioteca');
    return (
      <Provider store={store}>
        <AppWithNavigation />
      </Provider>
    );
  }
}

AppRegistry.registerComponent('ProjectB', () => Biblioteca);

export default Biblioteca;
