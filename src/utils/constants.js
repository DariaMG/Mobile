export const AUTH_IN_PROGRESS = 'AUTH_IN_PROGRESS';
export const AUTH_SUCCEEDED = 'AUTH_SUCCEEDED';
export const AUTH_FAILED = 'AUTH_FAILED';

export const BOOKS = 'BOOKS';
export const ONEBOOK = 'ONEBOOK';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
