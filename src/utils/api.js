export const apiUrl = '10.0.2.2:3000';
export const httpApiUrl = `http://${apiUrl}`;
export const wsApiUrl = `ws://${apiUrl}`;
export const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const authHeaders = (token) => ({ ...headers, Authorization: `Bearer ${token}` });
