import { combineReducers } from 'redux';

import nav from './nav';
import auth from './auth';
import books from './books';

const AppReducer = combineReducers({
  nav,
  auth,
  books,
});

export default AppReducer;
