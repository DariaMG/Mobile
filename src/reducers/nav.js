import { NavigationActions } from 'react-navigation';

// import { LoginNavigator } from './../service/app';
import { LoginNavigator } from './../services/app';


// Start with two routes: The Main screen, with the Login screen on top.
const firstAction = LoginNavigator.router.getActionForPathAndParams('Login');
const tempNavState = LoginNavigator.router.getStateForAction(firstAction);
// const secondAction = AppNavigator.router.getActionForPathAndParams('Main');
const initialNavState = LoginNavigator.router.getStateForAction(tempNavState);

export default (state = initialNavState, action) => {
  console.log(initialNavState);
  let nextState;
  switch (action.type) {
    case 'Login':
      nextState = LoginNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Home' }),
        state,
      );
      break;
    case 'Logout':
      nextState = LoginNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Login' }),
        state,
      );
      break;
    default:
      nextState = LoginNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
};
