import {
  BOOKS,
  ONEBOOK,
} from './../utils/constants';

const initialState = {
  books: [],
  book: {},
};

export default (state = initialState, action) => {
  console.log(`${action} ${JSON.stringify(state)}`);
  switch (action.type) {
    case BOOKS:
      return { books: action.books };
    case ONEBOOK:
      return { book: action.oneBook };
    default:
      return state;
  }
};
