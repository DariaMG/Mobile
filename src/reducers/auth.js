import {
  AUTH_IN_PROGRESS,
  AUTH_SUCCEEDED,
  AUTH_FAILED,
  LOGIN,
  LOGOUT,
} from './../utils/constants';

const initialState = {
  token: null,
  inprogress: false,
  issue: null,
  isLoggedIn: false,
};

export default (state = initialState, action) => {
  console.log(`${action.type} ${JSON.stringify(state)}`);
  switch (action.type) {
    case AUTH_IN_PROGRESS:
      return { token: null, inprogress: true };
    case AUTH_SUCCEEDED:
      return { token: action.payload.token, inprogress: false };
    case AUTH_FAILED:
      return { issue: action.payload.issue, inprogress: false };
    case LOGIN:
      return { isLoggedIn: true };
    case LOGOUT:
      return { isLoggedIn: false, token: null };
    default:
      return state;
  }
};

