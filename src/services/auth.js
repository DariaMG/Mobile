import { headers } from '../utils/api';
import { API_URL, request } from './routes';

export const login = ({ username, password }) => {
  const url = API_URL.POST.LOGIN;
  console.log(`------------------------getToken ${API_URL.POST.LOGIN}`);
  const options = {
    method: 'POST',
    body: JSON.stringify({
      username,
      password,
    }),
  };
  console.log(options);
  return request(url, headers, options);
};

export default login;
