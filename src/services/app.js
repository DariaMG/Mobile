// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  * @flow
//  */

// import React, { Component } from 'react';
// import {
//   Platform,
//   StyleSheet,
//   Text,
//   View
// } from 'react-native';
// import { HomePage } from './homePage';
// // import LogIn from './logIn';

// const instructions = Platform.select({
//   ios: 'Press Cmd+R to reload,\n' +
//     'Cmd+D or shake for dev menu',
//   android: 'Double tap R on your keyboard to reload,\n' +
//     'Shake or press menu button for dev menu',
// });

// export default class App extends Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <HomePage />
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
// });

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, StackNavigator } from 'react-navigation';

import Edit from '../component/edit';
import LogIn from '../component/logIn';
import Biblioteca from '../component/homePage';


export const AppNavigator = StackNavigator({
  Main: {
    screen: Biblioteca,
    navigationOption: {
      title: 'MyScreen',
      headerLeft: null,
    },
  },
  Edit: {
    screen: Edit,
    navigationOption: {
      title: ({ book }) => `${book.name}`,
    },
  },
}, {
  mode: 'modal',
  headerMode: 'none',
  headerLeft: null,
});

export const LoginNavigator = StackNavigator({
  Login: {
    screen: LogIn,
    navigationOption: {
      title: 'Please login',
    },
  },
  Home: {
    screen: AppNavigator,
    navigationOption: {
      title: 'Welcome to your library',
    },
  },
});


const AppWithNavigation = ({ dispatch, nav }) => {
  console.log(nav);
  return (
    <LoginNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
  );
};

AppWithNavigation.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.object.isRequired,
};

// const mapStateToProps = state => ({
//   nav: state.nav,
// });

const mapStateToProps = state => {
  console.log('------------------------');
  console.log(state);
  return {
    nav: state.nav,
  };
};

export default connect(mapStateToProps)(AppWithNavigation);
