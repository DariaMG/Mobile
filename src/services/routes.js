import { httpApiUrl } from '../utils/api';

const API_BASE_URL = `${httpApiUrl}/api`;
export const API_URL = {
  POST: {
    LOGIN: `${httpApiUrl}/login`,
  },
};


export const request = (url, headers, options) => {
  const augmentedOptions = Object.assign(options || {}, headers);
  console.log('-----------------------');
  console.log(augmentedOptions);
  return new Promise((resolve, reject) => {
    fetch(url, augmentedOptions)
      .then(response => {
        console.log(response);
        if (response.ok) {
          return response.json();
        }

        return reject(response);
      })
      .then(resolve)
      .catch(reject);
  });
};

