import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});


export class EditScreen extends Component {
  static propTypes = {
    oneBook: PropTypes.node,
    navigation: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = { nume: this.props.oneBook.nume, autor: this.props.oneBook.autor };
  }

  goBack = () => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'Home',
      // params: { book: item },
      // action: NavigationActions.navigate({ routeName: 'Edit' }),
    });
    this.props.navigation.dispatch(navigateAction);
  }


  render() {
    console.log(this.state.isLoading);
    console.log(this.state.issue);

    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Edit Screen
        </Text>
        <Button
          onPress={this.goBack}
          title="Go back"
        />
        <View>
          <Text> Nume </Text>
          <TextInput
            style={{
              height: 40,
              borderColor: 'black',
              borderWidth: 1,
              width: 150,
              backgroundColor: 'white',
              margin: 10,
              }}
            onChangeText={(nume) => this.setState({ nume })}
            value={this.state.nume}
          />
          <Text> Autor  </Text>
          <TextInput
            style={{
              height: 40,
              borderColor: 'black',
              borderWidth: 1,
              width: 150,
              backgroundColor: 'white',
              margin: 10,
              }}
            onChangeText={(autor) => this.setState({ autor })}
            value={this.state.autor}
          />
        </View>
      </View>
    );
  }
}

EditScreen.navigationOptions = {
  title: 'Edit',
};

const mapStateToProps = state => ({
  oneBook: state.books.book,
});

export default connect(mapStateToProps)(EditScreen);
