import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, FlatList } from 'react-native';
import { List, ListItem } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';

import LoginStatusMessage from './loginMessage';
import AuthButton from './authButton';

import { httpApiUrl, wsApiUrl } from '../utils/api';

import {
  ONEBOOK,
} from './../utils/constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});

const bookss = [
  { id: 1, nume: 'Amintiri din copilarie', autor: 'Ion Creanga' },
  { id: 2, nume: 'Amintiri din copilarie', autor: 'Ion Creanga' },
  { id: 3, nume: 'Amintiri din copilarie', autor: 'Ion Creanga' },
];

export class MainScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = { books: bookss, isLoading: false, issue: null };
  }

  componentDidMount() {
    console.log('componentDidMount');
    // this.setState({ isLoading: true });
    fetch(`${httpApiUrl}/books`)
      .then(response => response.json())
      .then(responseJson => this.setState({ books: responseJson.books, isLoading: false }))
      .catch(error => this.setState({ issue: { error: error.message }, isLoading: false }));
    this.connectWs();
  }

  onEditBook = (item) => {
    console.log(item);
    this.props.navigation.dispatch({
      type: ONEBOOK,
      oneBook: item,
    });
    // this.props.navigation.navigate('Edit', item);
    const navigateAction = NavigationActions.navigate({
      routeName: 'Edit',
      params: { book: item },
      // action: NavigationActions.navigate({ routeName: 'Edit' }),
    });
    this.props.navigation.dispatch(navigateAction);
    // NavigationActions.navigate({ routeName: 'Edit' });
  }

  onNewNote(e) {
    const book = JSON.parse(e.data).book;
    const { books } = this.state;
    this.setState({
      books: books ? books.concat([book]) : [book],
    });
  }

  connectWs() {
    const ws = new WebSocket(wsApiUrl);
    ws.onopen = () => console.log('onopen');
    ws.onmessage = this.onNewNote.bind(this);
    ws.onerror = e => console.log(e.message);
    ws.onclose = e => console.log(e.code);
  }

  render() {
    console.log(this.state.isLoading);
    console.log(this.state.issue);

    return (
      <View style={styles.container}>
        <View>
          <LoginStatusMessage />
          <AuthButton />
        </View>
        <View style={styles.container}>
          <List containerStyle={{
            borderTopWidth: 0,
            borderBottomWidth: 0,
            width: 350,
            height: '100%',
            }}
          >
            <FlatList
              data={this.state.books}
              renderItem={({ item }) => (
                <ListItem
                  key={item.id}
                  roundAvatar
                  title={item.nume}
                  subtitle={item.autor}
                  // avatar={{ uri: item.picture.thumbnail }}
                  containerStyle={{ borderBottomWidth: 0 }}
                  onPress={() => this.onEditBook(item)}
                />
              )}
              keyExtractor={item => item.id}
              ItemSeparatorComponent={this.renderSeparator}
              // onRefresh={this.handleRefresh}
              // refreshing={this.state.refreshing}
              // onEndReached={this.handleLoadMore}
              // onEndReachedThreshold={50}
            />
          </List>
        </View>
      </View>
    );
  }
}

MainScreen.navigationOptions = {
  title: 'Home Screen',
};

export default MainScreen;
