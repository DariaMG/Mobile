import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, StyleSheet, Text, View, TextInput } from 'react-native';
import { NavigationActions } from 'react-navigation';

import { login } from '../services/auth';

import {
  LOGIN,
} from './../utils/constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});


class LoginScreen extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    navigation: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      err: '',
    };
  }

  login = () => {
    // this.props.navigation.dispatch(login({
    //   username: this.state.username,
    //   password: this.state.password,
    // }));
    if (this.state.username === 'aaa' && this.state.password === 'aaa') {
      this.props.navigation.dispatch({
        type: LOGIN,
      });
      this.props.navigation.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'Home' }),
        ],
      }));
    } else {
      const err = 'Username or password incorrest!';
      this.setState({ err });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Text> Username </Text>
          <TextInput
            style={{
              height: 40,
              borderColor: 'black',
              borderWidth: 1,
              width: 150,
              backgroundColor: 'white',
              margin: 10,
              }}
            onChangeText={(username) => this.setState({ username })}
            value={this.state.username}
          />
          <Text> Password  </Text>
          <TextInput
            style={{
              height: 40,
              borderColor: 'black',
              borderWidth: 1,
              width: 150,
              backgroundColor: 'white',
              margin: 10,
              }}
            onChangeText={(password) => this.setState({ password })}
            value={this.state.password}
          />
        </View>
        {/* <Button
          onPress={() => this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({ routeName: 'Home' }),
            ],
          }))}
          title="Log in"
        /> */}
        <Button
          onPress={this.login}
          title="Log in"
        />
        {this.state.err ? <Text>{this.state.err}</Text> : null }
      </View>
    );
  }
}

LoginScreen.navigationOptions = {
  title: 'Log In',
};

export default LoginScreen;

